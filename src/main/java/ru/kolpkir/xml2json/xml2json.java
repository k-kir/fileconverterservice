package ru.kolpkir.xml2json;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.kolpkir.model.*;

public class xml2json {
    public static radios ReadFromXML(String pathToXML) throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        String readContent = new String(Files.readAllBytes(Paths.get(pathToXML)));
        return xmlMapper.readValue(readContent, radios.class);
    }

    public static artistsJSON ConvertToJSON(radios radioList) {
        artistsJSON artists = new artistsJSON();
        for (radio rad : radioList.radios) {
            for (artistXML art : rad.artists) {
                if (!artists.isArtist(art.name))
                    artists.add(art.name);
                for (String song : art.songs)
                    artists.addSongIfNot(art.name, song, rad.name);
            }
        }
        return artists;
    }

    public static void WriteToJSON(artistsJSON artists, String pathToJSON) throws IOException {
        FileWriter writer = new FileWriter(pathToJSON, false);
        writer.write(new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(artists));
        writer.flush();
    }
}
