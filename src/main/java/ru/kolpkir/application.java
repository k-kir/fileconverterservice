package ru.kolpkir;

import ru.kolpkir.json2xml.json2xml;
import ru.kolpkir.model.artistsJSON;
import ru.kolpkir.model.radios;
import ru.kolpkir.xml2json.xml2json;

import java.nio.file.Files;
import java.nio.file.Path;

public class application {
    public static void main(String[] args) {
        try {
            if (args.length != 2) {
                throw new Exception("You should pass two parameters: input file and output file. Exiting.");
            }
            String inputFile = args[0];
            String outputFile = args[1];
            if (inputFile.endsWith(".xml")) {
                if (outputFile.endsWith(".json")) {
                    if (Files.exists(Path.of(inputFile))) {
                        radios radioList = xml2json.ReadFromXML(inputFile);
                        artistsJSON artistsList = xml2json.ConvertToJSON(radioList);
                        xml2json.WriteToJSON(artistsList, outputFile);
                    } else {
                        throw new Exception("File does not exists.");
                    }
                } else {
                    throw new Exception("Program only can convert xml to json and json to xml.");
                }
            } else if (inputFile.endsWith(".json")) {
                if (outputFile.endsWith(".xml")) {
                    if (Files.exists(Path.of(inputFile))) {
                        artistsJSON artistsList = json2xml.ReadFromJSON(inputFile);
                        radios radioList = json2xml.ConvertToXML(artistsList);
                        json2xml.WriteToXML(radioList, outputFile);
                    } else {
                        throw new Exception("File does not exists.");
                    }
                } else {
                    throw new Exception("Program only can convert xml to json and json to xml.");
                }
            } else {
                throw new Exception("Program only can convert xml to json and json to xml.");
            }
        } catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }
}
