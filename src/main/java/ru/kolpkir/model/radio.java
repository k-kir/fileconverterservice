package ru.kolpkir.model;

import com.fasterxml.jackson.dataformat.xml.annotation.*;

import java.util.ArrayList;

public class radio {
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    public String name;

    @JacksonXmlElementWrapper(localName = "artists")
    public ArrayList<artistXML> artists;

    public radio() {
        artists = new ArrayList<artistXML>();
    }

    public radio(String name) {
        this.name = name;
        artists = new ArrayList<artistXML>();
    }

    public boolean isArtist(String artistName) {
        for (artistXML artist : artists)
            if (artist.name.equals(artistName))
                return true;
        return false;
    }

    public void addSongIfNot(String artistName, String songName) {
        for (artistXML artist : artists) {
            if (artist.name.equals(artistName)) {
                if (artist.getIndexOfSong(songName) == -1)
                    artist.songs.add(songName);
                break;
            }
        }
    }
}
