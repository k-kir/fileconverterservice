package ru.kolpkir.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class song {
    @JsonProperty("name")
    public String name;
    public ArrayList<String> radios;

    public song() {
        radios = new ArrayList<String>();
    }

    public song(String name, String radio) {
        this.name = name;
        radios = new ArrayList<String>();
        radios.add(radio);
    }
}
