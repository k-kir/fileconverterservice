package ru.kolpkir.model;

import java.util.ArrayList;

public class artistsJSON {
    public ArrayList<artistJSON> artists;

    public artistsJSON() {
        artists = new ArrayList<artistJSON>();
    }

    public boolean isArtist(String name) {
        for (artistJSON artist : artists)
            if (artist.name.equals(name))
                return true;
        return false;
    }

    public void add(String name) {
        artists.add(new artistJSON(name));
    }

    public void addSongIfNot(String artistName, String songName, String radioName) {
        for (artistJSON artist : artists)
            if (artist.name.equals(artistName)) {
                if (artist.getIndexOfSongName(songName) == -1)
                    artist.songs.add(new song(songName, radioName));
                else
                    artist.songs.get(artist.getIndexOfSongName(songName)).radios.add(radioName);
                break;
            }
    }
}
