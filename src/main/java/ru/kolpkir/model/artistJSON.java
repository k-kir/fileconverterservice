package ru.kolpkir.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class artistJSON {
    @JsonProperty("name")
    public String name;
    public ArrayList<song> songs;

    public artistJSON() {
        songs = new ArrayList<song>();
    }

    public artistJSON(String name) {
        this.name = name;
        songs = new ArrayList<song>();
    }

    public int getIndexOfSongName(String songName) {
        for (int i = 0; i < songs.size(); i++) {
            if (songs.get(i).name.equals(songName))
                return i;
        }
        return -1;
    }
}
