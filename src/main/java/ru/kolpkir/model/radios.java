package ru.kolpkir.model;

import com.fasterxml.jackson.dataformat.xml.annotation.*;

import java.util.ArrayList;

public class radios {
    @JacksonXmlElementWrapper(localName = "radios")
    public ArrayList<radio> radios;

    public radios() {
        radios = new ArrayList<radio>();
    }

    public boolean isRadio(String radioName) {
        for (radio rad : radios)
            if (rad.name.equals(radioName))
                return true;
        return false;
    }

    public void add(String radioName) {
        radios.add(new radio(radioName));
    }

    public int getIndexOfRadio(String radioName) {
        for (int i = 0; i < radios.size(); i++)
            if (radios.get(i).name.equals(radioName))
                return i;
        return -1;
    }
}
