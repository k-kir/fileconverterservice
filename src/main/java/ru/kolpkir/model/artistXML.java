package ru.kolpkir.model;

import com.fasterxml.jackson.dataformat.xml.annotation.*;

import java.util.ArrayList;

public class artistXML {
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    public String name;

    @JacksonXmlElementWrapper(localName = "songs")
    public ArrayList<String> songs;

    public artistXML() {
        songs = new ArrayList<String>();
    }

    public artistXML(String name) {
        this.name = name;
        songs = new ArrayList<String>();
    }

    public int getIndexOfSong(String songName) {
        for (int i = 0; i < songs.size(); i++) {
            if (songs.get(i).equals(songName))
                return i;
        }
        return -1;
    }
}
