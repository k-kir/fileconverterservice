package ru.kolpkir.json2xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.kolpkir.model.*;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class json2xml {
    public static artistsJSON ReadFromJSON(String pathToJSON) throws IOException {
        String readContent = new String(Files.readAllBytes(Paths.get(pathToJSON)));
        return new ObjectMapper().readValue(readContent, artistsJSON.class);
    }

    public static radios ConvertToXML(artistsJSON artistsList) {
        radios rads = new radios();
        for (artistJSON artist : artistsList.artists) {
            for (song sg : artist.songs) {
                for (String rad : sg.radios) {
                    if (!rads.isRadio(rad))
                        rads.add(rad);
                    if (!rads.radios.get(rads.getIndexOfRadio(rad)).isArtist(artist.name))
                        rads.radios.get(rads.getIndexOfRadio(rad)).artists.add(new artistXML(artist.name));
                    rads.radios.get(rads.getIndexOfRadio(rad)).addSongIfNot(artist.name, sg.name);
                }
            }
        }
        return rads;
    }

    public static void WriteToXML(radios rads, String pathToXML) throws IOException {
        FileWriter writer = new FileWriter(pathToXML, false);
        writer.write(new XmlMapper().writerWithDefaultPrettyPrinter().writeValueAsString(rads));
        writer.flush();
    }
}
